// try mo nga ilabas ung logic sa functions... para pde i call na lang

// controllers contain the functions and business logic of our express JS meaning all the operation it can do will be placed here

const User = require("../models/user");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
// const ACCESS_TOKEN_SECRET = require("../password").ACCESS_TOKEN_SECRET;
require("dotenv").config();

module.exports.signIn = async (req) => {
  let hashedPassword = await User.findOne({
    email: req.body.email,
  });
  console.log(`req.body.email is ${req.body.email}`);
  console.log(`req.body.password is ${req.body.password}`);
  console.log(`hashedPassword is ${hashedPassword}`);
  passwordCheck = bcrypt.compareSync(
    req.body.password,
    hashedPassword.password
  );
  if (passwordCheck) {
    req.body.password = hashedPassword.password;
    const accessToken = jwt.sign(req.body, process.env.ACCESS_TOKEN_SECRET);
    return JSON.stringify({ accessToken: accessToken });
  } else {
    return "password do not match";
  }
};

module.exports.getAll = async () => {
  return await User.find({});
};
module.exports.getProfile = async (userID) => {
  return await User.find({ _id: userID });
};

// module.exports.findEmail = async (req) => {
//   return await User.find({ email: req.body.email });
// };

module.exports.createNew = async (req) => {
  if ((await User.find({ email: req.body.email })).length > 0) {
    return "email account already exists";
  } else {
    let newUser = new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 10),
      isAdmin: req.body.isAdmin,
      mobileNo: req.body.mobileNo,
      enrollments: req.body.enrollments,
    });
    try {
      return await newUser.save();
    } catch (err) {
      return err;
    }
  }
};

// module.exports.deleteByName = async (req) => {
//   let toDelete = await Task.findOne({ name: req.body.name });
//   return await toDelete.remove();
// };

// module.exports.deleteByID = async (taskID) => {
//   let toDelete = await Task.findOne({ _id: taskID });
//   return await toDelete.remove();
// };

// module.exports.updateTask = async (req) => {
//   let toUpdate = await Task.findOne(req.body.filter);
//   return await toUpdate.update(req.body.update);
// };

// module.exports.updateByID = async (taskID, req) => {
//   let toUpdate = await Task.findOne({ _id: taskID });
//   console.log(req.body.update);
//   await toUpdate.update(req.body.update);
//   return toUpdate.save();
// };
