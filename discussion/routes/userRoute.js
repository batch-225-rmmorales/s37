const express = require("express");
const userController = require("../controllers/userController");
const router = express.Router();

router.get("/", async (req, res) => {
  result = await userController.getAll();
  res.send(result);
});

router.get("/details/:id", async (req, res) => {
  result = await userController.getProfile(req.params.id);
  res.send(result);
});

router.post("/signup", async (req, res) => {
  result = await userController.createNew(req);
  res.send(result);
});

router.post("/signin", async (req, res) => {
  result = await userController.signIn(req);
  res.send(result);
});

module.exports = router;
