const express = require("express");
const courseController = require("../controllers/courseController");
//create a router instance that functions as middle ware and routing system
// allows access to http method middlewares taht makes it e4asier to creat routes
const router = express.Router();
const jwt = require("jsonwebtoken");
const User = require("../models/user");

async function authenticateToken(req, res, next) {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  // no authentication token
  if (token == null) return res.sendStatus(401);
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) {
      // token not verified
      return res.sendStatus(403);
    }
    //check if authorized
    req.user = user;
  });
  let passwordCheck =
    (
      await User.find({
        email: req.user.email,
        password: req.user.password,
      })
    ).length > 0;
  if (passwordCheck) {
    console.log(passwordCheck);
    console.log(req.user);

    next();
  } else {
    // user not found
    return res.sendStatus(401);
  }
}

// section router
router.get("/", async (req, res) => {
  result = await courseController.getAll();
  res.send(result);
});

router.post("/create", authenticateToken, async (req, res) => {
  result = await courseController.createNew(req);
  res.send(result);
});

router.put("/update/:id", authenticateToken, async (req, res) => {
  result = await courseController.updateByID(req.params.id, req.body.update);
  // console.log(req.body.update);
  res.send(result);
});

router.put("/archive/:id", async (req, res) => {
  result = await courseController.archiveByID(req.params.id);
  res.send(result);
});

// router.delete("/", async (req, res) => {
//   resultFromController = await taskController.deleteByName(req);
//   res.send(resultFromController);
// });

// router.delete("/delete/:id", async (req, res) => {
//   resultFromController = await taskController.deleteByID(req.params.id);
//   res.send(resultFromController);
// });

// router.put("/", async (req, res) => {
//   resultFromController = await taskController.updateTask(req);
//   res.send(resultFromController);
// });

module.exports = router;
