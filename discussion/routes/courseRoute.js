const express = require("express");
const courseController = require("../controllers/courseController");
//create a router instance that functions as middle ware and routing system
// allows access to http method middlewares taht makes it e4asier to creat routes
const router = express.Router();
const jwt = require("jsonwebtoken");
const User = require("../models/user");
const bcrypt = require("bcrypt");

async function authenticateToken(req, res, next) {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];

  // no authentication token
  if (token == null) return res.sendStatus(401);
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) {
      // token not verified
      return res.sendStatus(403);
    }
    //check if authorized
    req.user = user;
    //https://stackoverflow.com/questions/32633561/cant-access-object-property-of-a-mongoose-response
    console.log(req.user.password);
  });
  let myUser = await User.findOne({
    email: req.user.email,
  });
  console.log(myUser.password);

  let passwordCheck = myUser.password == req.user.password;
  console.log(passwordCheck);
  if (passwordCheck) {
    next();
  } else {
    //wrong status password dont match
    return res.sendStatus(401);
  }
}

router.get("/", async (req, res) => {
  result = await courseController.getAll();
  res.send(result);
});

router.post("/create", authenticateToken, async (req, res) => {
  result = await courseController.createNew(req);
  res.send(result);
});

router.put("/update/:id", authenticateToken, async (req, res) => {
  result = await courseController.updateByID(req.params.id, req.body.update);
  // console.log(req.body.update);
  res.send(result);
});

router.put("/archive/:id", async (req, res) => {
  result = await courseController.archiveByID(req.params.id);
  res.send(result);
});

module.exports = router;
