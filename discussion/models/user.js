//create the schema,  model and export the file

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, "username is required"],
  },
  email: {
    type: String,
    required: [true, "Email is required"],
  },
  password: {
    type: String,
    required: [true, "Password is required"],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  mobileNo: {
    type: String,
  },
  enrollments: [
    {
      courseId: {
        type: String,
        required: [true, "course id is required"],
      },
      enrolledOn: {
        type: Date,
        default: new Date(),
      },
      status: {
        //try mo gamit enum dito
        type: String,
        default: "Enrolled",
      },
    },
  ],
});

// module.exports is the way for node js to treat this value as package
module.exports = mongoose.model("User", userSchema);
