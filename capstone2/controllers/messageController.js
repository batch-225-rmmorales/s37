// try mo nga ilabas ung logic sa functions... para pde i call na lang

// controllers contain the functions and business logic of our express JS meaning all the operation it can do will be placed here

const Message = require("../models/Message");

module.exports.getAll = async () => {
  // return statement. returns the result of the mongoose method
  //the then method is used to wait for the mongoose method to finish before sending the result back to the route.
  return await Course.find({});
};

module.exports.createNew = async (req) => {
  let newMessage = new Message({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    isActive: req.body.isActive,
    enrollees: req.body.enrollees,
  });

  return await newMessage.save();
};
