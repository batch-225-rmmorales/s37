// try mo nga ilabas ung logic sa functions... para pde i call na lang

// controllers contain the functions and business logic of our express JS meaning all the operation it can do will be placed here

const User = require("../models/user");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
// const ACCESS_TOKEN_SECRET = require("../password").ACCESS_TOKEN_SECRET;
require("dotenv").config();


module.exports.signup = async (req) => {
  if ((await User.find({ email: req.body.email })).length > 0) {
    return "email account already exists";
  } else {
    let newUser = new User({
      username: req.body.username,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 10),
      mobileNo: req.body.mobileNo,
      address: req.body.address, 
    });
    try {
      return await newUser.save();
    } catch (err) {
      return err;
    }
  }
};

module.exports.signin = async (req) => {
  let hashedPassword = await User.findOne({
    email: req.body.email,
  });
  console.log(`req.body.email is ${req.body.email}`);
  console.log(`req.body.password is ${req.body.password}`);
  console.log(`hashedPassword is ${hashedPassword}`);
  passwordCheck = bcrypt.compareSync(
    req.body.password,
    hashedPassword.password
  );
  if (passwordCheck) {
    req.body.password = hashedPassword.password;
    req.body.userID = hashedPassword._id;
    const accessToken = jwt.sign(req.body, process.env.ACCESS_TOKEN_SECRET);
    return JSON.stringify({ accessToken: accessToken });
  } else {
    return "password do not match";
  }
};

module.exports.getProfile = async (username) => {
  return await User.find({ username: username });
};

module.exports.getFriends = async (username) => {
  return await User.find({ username: username }).friends;
};


module.exports.getAll = async () => {
  return await User.find({});
};

// module.exports.findEmail = async (req) => {
//   return await User.find({ email: req.body.email });
// };


// module.exports.deleteByName = async (req) => {
//   let toDelete = await Task.findOne({ name: req.body.name });
//   return await toDelete.remove();
// };

// module.exports.deleteByID = async (taskID) => {
//   let toDelete = await Task.findOne({ _id: taskID });
//   return await toDelete.remove();
// };

// module.exports.updateTask = async (req) => {
//   let toUpdate = await Task.findOne(req.body.filter);
//   return await toUpdate.update(req.body.update);
// };

// module.exports.updateByID = async (taskID, req) => {
//   let toUpdate = await Task.findOne({ _id: taskID });
//   console.log(req.body.update);
//   await toUpdate.update(req.body.update);
//   return toUpdate.save();
// };
