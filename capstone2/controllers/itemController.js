// try mo nga ilabas ung logic sa functions... para pde i call na lang

// controllers contain the functions and business logic of our express JS meaning all the operation it can do will be placed here

const Item = require("../models/item");

module.exports.getAll = async () => {
  // return statement. returns the result of the mongoose method
  //the then method is used to wait for the mongoose method to finish before sending the result back to the route.
  return await Item.find({});
};

module.exports.createNew = async (req) => {
  let newItem = new Item({

    userID: req.body.user.userID,
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    inventory: req.body.inventory,
  });

  return await newItem.save();
};
