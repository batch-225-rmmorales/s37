// const prompt = require("prompt-sync")();
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoute = require("./routes/userRoute");
const itemRoute = require("./routes/itemRoute");
const messageRoute = require("./routes/messageRoute");
// const commentRoute = require("./routes/commentRoute");

require("dotenv").config();

const app = express();
const port = 5000;
// CORS (Cross-Origin Resource Sharing) is a security feature implemented by web browsers that blocks web pages from making requests to a different domain than the one that served the web page. This is done to prevent malicious websites from stealing sensitive information from other websites
//In Node.js, CORS can be implemented using a middleware package such as cors. It allows you to configure the server to allow or disallow specific origins and HTTP methods. This can be useful if you are building a web application that makes API calls to a server running on a different domain.
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

let db = mongoose.connection;
console.log(process.env.PASSWORD);
mongoose.connect(
  "mongodb+srv://mors2:N0rmanrafael@cluster0.uvgjod2.mongodb.net/capstone?retryWrites=true&w=majority",
  //options in braces
  {
    // In simple words, "useNewUrlParser : true" allows us to avoid any current and future errors while connecting to MongoDB
    useNewUrlParser: true,

    // False by default. Set to true opt in to using MongoDB driver's new connection management engine. You shoult set this option to true, except for the unlikely case that it prevents you from mainting a stable connection
    useUnifiedTopology: true,
  }
);

// Setup notification for connection success or failure

db.on("error", console.error.bind(console, "connection error"));

db.on("open", () => console.log("connected to mongodb"));

app.use("/api/user", userRoute);
app.use("/api/item", itemRoute);
app.use("/api/message", messageRoute);
// app.use("api/comments", commentRoute);

app.listen(port, () => console.log(`listening port ${port}`));
 