# CAPSTONE 2: Backend with Express and MongoDB
## Models
- User
    - username / displayname ✅
    - email ✅
    - password ✅
    - mobileNo ✅
    - address ✅
    - friends [array of userIDs] ✅
    - blocked [array of userIDs] ✅
    - reviews [] ✅
        - userID ✅
        - review ✅
        - rating ✅
    - properties ✅
        - settings ✅
            - notif ✅
            - privacy
        - isAdmin ✅
        
- Item
    - userID
    - name ✅
    - description ✅
    - price ✅
    - inventory ✅
    - likes [array of userIDs] ✅
    - viewed [array of userIDs] ✅
    - properties ✅
        - negotiable ✅
        - status (for sale, sold, archived) ✅
        - iat ✅
    

- Chat / Notification
    - fromID (if from system - notification) ✅
        - notifications (friend request, message)
    - toID ✅
    - properties
        - iat ✅
        - ~~status (unread, read)~~
        - isParent ✅ 
    - message ✅
    - views [array of userIDs] ✅
    - likes [array of userIDs] ✅

- ~~Comments~~
    - fromID ✅
    - toID ✅
    - properties
        - iat ✅
        - checkForChild (default: true) ✅
    - message ✅
    - likes [array of userIDs] ✅
- Search (?)
- External API
## Routes
- User
    - api/user/signin ✅
    - api/user/signup ✅
    - api/user/:username ✅
    - api/user/:username/chats
    - api/user/:username/friends ✅
    - api/user/:username/blocked
    - api/user/:username/reviews
- Item
    - api/item/create
        - auth user
    - api/item/:itemid
- Chat (can only chat with friends and not blocked)
- Comments (can only comment if not blocked)
## Controllers
- User
    - can login via username/email
- Item
- Chat
- Comments

## Backlog
- put mobile no and address in contacts

## Resources
- https://blog.logrocket.com/jwt-authentication-best-practices/
- https://stackoverflow.com/questions/41014427/how-to-add-data-to-request-body-while-proxying-it