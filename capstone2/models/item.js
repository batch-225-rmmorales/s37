// requires
const mongoose = require("mongoose");

const itemSchema = new mongoose.Schema({
  
  userID: {
    type: String,
    // required: [true, "userID is required"],
  },
  name: {
    type: String,
    required: [true, "Item name is required"],
  },
  description: {
    type: String,
    required: [true, "Description is required"],
  },
  price: {
    type: Number,
    required: [true, "Price is required"],
  },
  inventory: {
    type: Number,
    required: [true, "Inventory is required"],
  },
  properties: {
    negotiable: Boolean,
    status: String,
    iat: {
      type: Date,
      // The "new Date()" expression instantiates a new "date" that stores the current date and time whenever a course is created in our database
      default: new Date(),
    }
  },
  likes: [
    {userID: String}
  ],
  views: [
    {userID: String}
  ],
  
});

module.exports = mongoose.model("Item", itemSchema);
