//create the schema,  model and export the file

const mongoose = require("mongoose");

const messageSchema = new mongoose.Schema({
  fromID: {
    type: String,
    required: [true, "fromID is required"],
  },
  toID: {
    type: String,
    required: [true, "toID is required"],
  },
  message: {
    type: String,
    required: [true, "message is required"],
  },
  likes: [
    {
      userID: String
    },
  ],
  views: [
    {
      userID: String
    },
  ],
  properties: {
    iat: {
      type: Date,
      // The "new Date()" expression instantiates a new "date" that stores the current date and time whenever a course is created in our database
      default: new Date(),
    },
    isParent: {
      type: Boolean,
      default: false
    }

  }
  
    
});

// module.exports is the way for node js to treat this value as package
module.exports = mongoose.model("Message", messageSchema);
