//create the schema,  model and export the file

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, "username is required"],
  },
  email: {
    type: String,
    required: [true, "Email is required"],
  },
  password: {
    type: String,
    required: [true, "Password is required"],
  },
  address: {
    type: String,
    required: [true, "address is required"],
  },
  
  mobileNo: {
    type: String,
  },
  friends: [
    {
      userId: {
        type: String,
        required: [true, "userID is required"],
      }
    },
  ],
  blocked: [
    {
      userId: {
        type: String,
        required: [true, "userID is required"],
      }
    },
  ],
  reviews: [
    {
      userId: {
        type: String,
        required: [true, "userID is required"],
      },
      review: {
        type: String,
        required: [true, "review is required"],
      },
      rating: {
        type: Number,
        required: [true, "rating is required"],
      }
    },
  ],
  properties: {
    settings: {
      notifications: String
    },
    isAdmin: {
      type: Boolean,
      default: false,
    },   
  },
  
});

// module.exports is the way for node js to treat this value as package
module.exports = mongoose.model("User", userSchema);
