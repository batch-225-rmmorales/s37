const express = require("express");
const itemController = require("../controllers/itemController");
const router = express.Router();

const jwt = require("jsonwebtoken");
const User = require("../models/user");
const bcrypt = require("bcrypt");

async function authenticateToken(req, res, next) {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];

  // no authentication token
  if (token == null) return res.sendStatus(401);
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) {
      // token not verified
      return res.sendStatus(403);
    }
    //check if authorized
    req.body.user = user;
    console.log(req.body.user);
    //https://stackoverflow.com/questions/32633561/cant-access-object-property-of-a-mongoose-response
    console.log(req.body.user.password);
  });
  let myUser = await User.findOne({
    email: req.body.user.email,
  });
  console.log(myUser.password);

  let passwordCheck = myUser.password == req.body.user.password;
  console.log(passwordCheck);
  if (passwordCheck) {
    // res.send(req.user);
    next();
  } else {
    //wrong status password dont match
    return res.sendStatus(401);
  }
}

router.get("/", async (req, res) => {
  result = await itemController.getAll();
  res.send(result);
});

router.post("/create", authenticateToken, async (req, res) => {
  result = await itemController.createNew(req);
  res.send(result);
});

module.exports = router;
