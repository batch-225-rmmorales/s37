const express = require("express");
const userController = require("../controllers/userController");
const router = express.Router();


router.post("/signup", async (req, res) => {
  result = await userController.signup(req);
  res.send(result);
});

router.post("/signin", async (req, res) => {
  result = await userController.signin(req);
  res.send(result);
});

router.get("/", async (req, res) => {
  result = await userController.getAll();
  res.send(result);
});

router.get("/:username", async (req, res) => {
  result = await userController.getProfile(req.params.username);
  res.send(result);
});

router.get("/:username/friends", async (req, res) => {
  result = await userController.getFriends(req.params.username);
  res.send(result);
});


module.exports = router;
