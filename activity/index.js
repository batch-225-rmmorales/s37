const prompt = require("prompt-sync")();
const express = require("express");
const mongoose = require("mongoose");
// mongoose.set("strictQuery", false);
const taskRoute = require("./routes/taskRoute");

let PASSWORD = require("./password").PASSWORD;

const app = express();
const port = 5000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

let db = mongoose.connection;

mongoose.connect(
  `mongodb+srv://mors2:${PASSWORD}@cluster0.uvgjod2.mongodb.net/s36?retryWrites=true&w=majority`,
  //options in braces
  {
    // In simple words, "useNewUrlParser : true" allows us to avoid any current and future errors while connecting to MongoDB
    useNewUrlParser: true,

    // False by default. Set to true opt in to using MongoDB driver's new connection management engine. You shoult set this option to true, except for the unlikely case that it prevents you from mainting a stable connection
    useUnifiedTopology: true,
  }
);

// Setup notification for connection success or failure

db.on("error", console.error.bind(console, "connection error"));

db.on("open", () => console.log("connected to mongodb"));

// ad the task routes
// allows all the task routes created in the taskroutes file to use in the app
app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`listening port ${port}`));
